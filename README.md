
cubicFUSION loadBalancer
=====================================
* * *

http://www.portalzine.de

About
-----

PHP Class to limit areas of a webpage to a certain amount of visitors.
- db or file based
- define amount of user seats
- waiting list for user
- as seats become available, user moves up until he can access the page

License
-----------

/LICENSE.txt.